import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailMotoPage } from './detail-moto.page';

const routes: Routes = [
  {
    path: '',
    component: DetailMotoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailMotoPageRoutingModule {}
