import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-detail-moto',
  templateUrl: './detail-moto.page.html',
  styleUrls: ['./detail-moto.page.scss'],
})
export class DetailMotoPage implements OnInit {

  id: any;

  public moto;
  sub: any;
  constructor(public alertController: AlertController,private route: ActivatedRoute) { 
    this.sub = this.route.params.subscribe(params => {
      this.id = params['model'];
    });

    this.getMotoById();
  }

  ngOnInit() {
  }

  


  async getMotoById(){
    const respuesta = await fetch(`http://andrea-valcarcel-7e2.alwaysdata.net/api-motos/getMoto/${this.id}`);
    this.moto = await respuesta.json();
    console.log(this.moto);
  }

  async deleteMoto(id){
    const url = `http://andrea-valcarcel-7e2.alwaysdata.net/api-motos/delmoto/` +id;
    fetch(url, {
      "method": "GET"
    })
    .then(response => {
      //this.getMotos();
      });

  }

  async alertDelete(idMoto) {
    const alert = await this.alertController.create({
      header: '¿Estas seguro de querer eliminar la moto?',
      message: 'Pulsa Confirmar para eliminar',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: (blah) => {
            console.log('Cancelado');
          }
        }, {
          text: 'Confirmar',
          handler: () => {
            this.deleteMoto(idMoto);
            console.log('Eliminado');
          }
        }
      ]
    });

    await alert.present();
  }
}

