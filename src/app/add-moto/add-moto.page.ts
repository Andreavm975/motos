import { Component, OnInit } from '@angular/core';
import { FormGroup, FormsModule, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-moto',
  templateUrl: './add-moto.page.html',
  styleUrls: ['./add-moto.page.scss'],
})
export class AddMotoPage implements OnInit {

  motoForm: FormGroup;

  constructor(private router:Router,public fb:FormBuilder) { 
    
  }

  ngOnInit() {
    this.motoForm = this.fb.group({
       foto:[''],
       marca:[''],
       model:[''],
       year:[''],
       preu:['']
    })
  }

  submit(){

    const url = `http://andrea-valcarcel-7e2.alwaysdata.net/api-motos/addmoto/`+this.motoForm.value.marca+`/`+this.motoForm.value.model+`/`+this.motoForm.value.year+`/`+this.motoForm.value.preu;
    fetch(url, {
      "method": "GET"
    })
    .then(response => {
      this.router.navigate(['/']);
      });
  }

}
