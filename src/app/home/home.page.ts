import { Component } from '@angular/core';
import { AlertController, MenuController } from '@ionic/angular';
import { menuController } from '@ionic/core/dist/ionic/index.esm.js';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public motos =[];
  public marcas= [{'foto':'assets/ducati.png','nombre':'Ducati'}, {'foto':'assets/yamaha.png','nombre':'Yamaha'},{'foto':'assets/honda.png','nombre':'Honda'}];
  public keys = Object.keys(this.marcas);
  constructor(private router:Router, public alertController: AlertController, public menuController: MenuController) {
    this.getMotos();
    menuController = menuController;

  }
  ngOnInit(){
    this.getMotos();
  }

  async openMenu() {
    await menuController.open();
  }

  async getMotos(){
    const respuesta = await fetch("http://andrea-valcarcel-7e2.alwaysdata.net/api-motos/getMotos");
    this.motos = await respuesta.json();
    console.log(this.motos);
    this.menuController.close();
  }

  async getMotosByMarca(marca:string){ 
    
    const respuesta = await fetch("http://andrea-valcarcel-7e2.alwaysdata.net/api-motos/getMotosMarca/"+marca);
    await console.log(respuesta);
    this.motos = await respuesta.json();
    console.log(this.motos);
    this.menuController.close();
  }

  detailMoto(id:string){
    this.router.navigate([`/detail-moto`, {id}]);
  }
  

  async deleteMoto(id){
    const url = `http://andrea-valcarcel-7e2.alwaysdata.net/api-motos/delmoto/` +id;
    fetch(url, {
      "method": "GET"
    })
    .then(response => {
      this.getMotos();
      });

  }

  async alertDelete(idMoto) {
    const alert = await this.alertController.create({
      header: '¿Estas seguro de querer eliminar la moto?',
      message: 'Pulsa Confirmar para eliminar',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: (blah) => {
            console.log('Cancelado');
          }
        }, {
          text: 'Confirmar',
          handler: () => {
            this.deleteMoto(idMoto);
            console.log('Eliminado');
          }
        }
      ]
    });

    await alert.present();
  }
}
export interface Moto {
  id:number,
  marca: string,
  modelo: string,
  year: string,
  foto: string,
  precio: string
}
