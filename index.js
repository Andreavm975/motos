const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser'); 
const app = express();
const baseUrl = '/api-motos';
app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//DataBase Config
ddbbConfig = {
   user: 'andrea-valcarcel-7e2',
   host: 'postgresql-andrea-valcarcel-7e2.alwaysdata.net',
   database: 'andrea-valcarcel-7e2_motos',
   password: 'Ripollet_97',
   port: 5432
};

const Pool = require('pg').Pool;
const pool = new Pool(ddbbConfig);

//Get all Motos
const getMotos = (request, response) => {
   var consulta = "SELECT * FROM motos"
   pool.query(consulta, (error, results) => {
       if (error) {
           throw error
       }
       response.status(200).json(results.rows)
       console.log(results.rows);
   });
}
app.get(baseUrl + '/getmotos', getMotos);

//Get Motos by Marca
const getMotosByMarca = (request, response) => {
   var marca = request.params.marca.charAt(0).toUpperCase() + request.params.marca.slice(1);
   var consulta = "SELECT * FROM motos WHERE marca='" +marca+"'";
   pool.query(consulta, (error, results) => {
       if (error) {
           throw error
       }
       response.status(200).json(results.rows)
       console.log(results.rows);
   });
}
app.get(baseUrl + `/getmotosmarca/:marca`, getMotosByMarca);

//Get Motos by Marca
const getMotosById = (request, response) => {
   var model = request.params.model.charAt(0).toUpperCase() + request.params.model.slice(1);
   var consulta = "SELECT * FROM motos WHERE model='" +model+"'";
   pool.query(consulta, (error, results) => {
       if (error) {
           throw error
       }
       response.status(200).json(results.rows)
       console.log(results.rows);
   });
}
app.get(baseUrl + `/getmoto/:id`, getMotosById);


//Add Moto
const addMoto = (request, response) => {
   const marca = request.params.marca;
   const model = request.params.model;
   const year = request.params.year;
   const preu = request.params.preu;

   var consulta= "INSERT INTO motos ( marca, model, year, preu) VALUES ($1, $2, $3, $4)"
   pool.query(consulta, [marca, model, year, preu],(error,response) =>{
      if (error) {
         throw error
      }
      console.log(marca, model, year, preu);
      response.send(JSON.stringify(request.body));
   });

   response.end();
}
app.get(baseUrl +`/addmoto/:marca/:model/:year/:preu`, addMoto);

//Delete Moto
const deleteMoto = (request, response) => {
   var consulta = "DELETE FROM motos WHERE id=" + request.params.id;
   pool.query(consulta, (error, results) =>{
      if (error) {
         throw error
     }
     response.status(200).json(results.rows)
     console.log(results.rows);
   });
}
app.get(baseUrl + `/delmoto/:id`, deleteMoto);



const PORT = process.env.PORT || 3000;
const IP = process.env.IP || null;

app.listen(PORT, IP, () => {
   console.log("El servidor está inicializado en el puerto " + PORT);
});
